import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyInspectionsPage } from './my-inspections.page';

const routes: Routes = [
  {
    path: '',
    component: MyInspectionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyInspectionsPageRoutingModule {}
