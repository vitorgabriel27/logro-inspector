import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Perfil', url: '/profile', icon: 'user' },
    { title: 'Minhas Inspeções', url: '/my-inspections', icon: 'eye' },
    { title: 'Dados Bancários', url: '/wallet', icon: 'usd' },

    
  ];

  public persons = [
    {
      name: 'Luiza Salvatori',
      rate: 4.90,
    },
  
  ];
  
  constructor( private menu: MenuController) {}

  closeMenu() {
    this.menu.close('menu');
  }
}
