import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FRequestPageRoutingModule } from './f-request-routing.module';

import { FRequestPage } from './f-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FRequestPageRoutingModule
  ],
  declarations: [FRequestPage]
})
export class FRequestPageModule {}
