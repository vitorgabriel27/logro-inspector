import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeginInspectionPageRoutingModule } from './begin-inspection-routing.module';

import { BeginInspectionPage } from './begin-inspection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeginInspectionPageRoutingModule
  ],
  declarations: [BeginInspectionPage]
})
export class BeginInspectionPageModule {}
