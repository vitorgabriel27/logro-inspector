import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeginInspectionPage } from './begin-inspection.page';

const routes: Routes = [
  {
    path: '',
    component: BeginInspectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeginInspectionPageRoutingModule {}
