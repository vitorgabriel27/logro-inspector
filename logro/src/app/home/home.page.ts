import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

import { NewInspectionPage } from '../new-inspection/new-inspection.page';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private menu: MenuController, public modalController:ModalController) { }

  ngOnInit() {
  }

  public notifications = [
    {
      title: 'Você tem 3 inspeções programadas para hoje.',
    },
    {
      title: 'Você tem uma nova solicitação de inspecção.',
    },
    {
      title: 'Você tem 3 inspeções programadas para hoje.',
    },
  ];

  public noteLength = this.notifications.length

  openMenu() {
    this.menu.open('menu');
  }

  openNotify() {
    this.menu.open('notify');
  }

  async openNewInspection() {
    const modal = await this.modalController.create({
      component: NewInspectionPage,
      cssClass: 'new-inspection-modal'
    });
    return await modal.present();
  }



}
